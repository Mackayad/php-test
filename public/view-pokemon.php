<?php

	if(!(isset($_GET['name']) and !empty($_GET['name']))){
		echo "Please provide a valid Pokemon name.";
		exit();
	}
	
	$result = file_get_contents("https://pokeapi.co/api/v2/pokemon/" . $_GET['name']);
	$data = json_decode($result, true);

?>

<?php include 'header.php'; ?>

    <body>
		<div class="row header-bar">
			<div class="col title">Pokedex</div>
			<div class="col"><button id="backButton"><i class="fas fa-long-arrow-alt-left"></i> Back</button></div>
		</div>
		<div class="container-fluid">  
      		<div class="row justify-content-md-center">  
        			<div class="col"> 
         			<h1 class="pokemon-name"><?php echo ucfirst($data['name']);?></h1>
        			</div>
	 		</div>
	 		<div class="row justify-content-md-center pokemon-data-row">
        			<div class="col-sm-4"> 
          			<img src="<?php echo $data['sprites']['front_default'];?>" border="0" class="pokemon-img">
        			</div>
				<div class="col-sm-4">
					<p><strong>Height: </strong> <?php echo ($data['height'] / 10) . "m"; ?></p>
					<p><strong>Weight: </strong> <?php echo ($data['weight'] / 10) . "kg"; ?></p>
					<p><strong>Species: </strong>
						<?php 
							$species_data = json_decode(file_get_contents($data['species']['url']), true);
							foreach($species_data['genera'] as $genera) {
								if($genera['language']['name'] == "en") {
									echo $genera['genus'];
								}
							}
						?>
					</p>
     			 </div>
       			<div class="col-sm-4">
        				<b>Abilities:</b>
         				<ul>
							<?php
           						 foreach ($data['abilities'] as $ability) {
              						echo "<li>".ucfirst($ability['ability']['name'])."</li>";
            						}
        						?>
        				</ul>   
      		</div>
		</div>
</body>
<script>
	$('#backButton').click(function() {
		window.location.href = "index.php";
	});
</script>