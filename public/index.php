<?php

	include 'PokemonData.php';
	
?>
<html lang="en">

	<?php include 'header.php'; ?>
	
	    <body>
		<div class="row header-bar">
			<div class="col title">Pokedex</div>
			<div class="col search-bar"><input type="text" class="col" placeholder="Enter a Pokemon name..." id="pokemonSearch"></div>
		</div>
		<div class="main-container">
			<div class="col main-content">
				<?php
					foreach($pokemon_names as $name) {
						echo "<a href='view-pokemon.php?name=".$name."'><div class='pokemon-box' data-name='".$name."'>" . ucfirst($name) . "</div></a>";
					}
				?>
			</div>
		</div>
    </body>
	<script>
		$('#pokemonSearch').on('keyup', function() {
			if($(this).val().length > 0) {
        	   		$('.pokemon-box').hide().filter("[data-name*='" + $(this).val().toLowerCase() + "']").show();
			} else {
				$('.pokemon-box').show();
			}
		});
	</script>
</html>