<?php
	
	// Initial call to get Pokemon count
	$fetch = file_get_contents("https://pokeapi.co/api/v2/pokemon/?limit=1");
	$fetch_result = json_decode($fetch, true);
	
	// Get all Pokemon data
	$fetch = file_get_contents("https://pokeapi.co/api/v2/pokemon/?limit=".$fetch_result['count']);
	$fetch_result = json_decode($fetch, true);
	
	// Store all Pokemon names in an array
	$pokemon_names = array();
	foreach($fetch_result['results'] as $pokemon) {
		array_push($pokemon_names, $pokemon["name"]);
	}
	
?>
	